var body_parser = require('body-parser');
var cookie_parser = require('cookie-parser');
var crypto = require('crypto');
var Customer = require('./models/customer.js');
var dotenv = require('dotenv').config();
var express = require('express');
var fs = require('fs');
var jwt = require('jsonwebtoken');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var passport = require('passport');
var path = require('path');
var querystring = require('querystring');
var request = require('request');
var session = require('express-session');
var sync = require('./sync')

mongoose.connect('mongodb://localhost/');

var app_title = process.env.APP_TITLE;
var app_uri = process.env.APP_URI;
var redirect_uri = process.env.REDIRECT_URI;
var app_api_key = process.env.APP_API_KEY;
var app_api_secret = process.env.APP_API_SECRET;
var scope = process.env.SCOPE;

var script_tag_script = './script_tag_script.js';
var vendor_credentials = './config/vendor_credentials.json';

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(body_parser.json());
app.use(body_parser.urlencoded({ extended : true }));
app.use(cookie_parser());
app.use(session({
    secret: 'foobarbaz',
    saveUninitialized: true,
    resave: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use('/styles', express.static(path.join(__dirname, '/public/styles')));
app.use('/scripts', express.static(path.join(__dirname, '/public/scripts')));

/*******************************************************************************
 *
 * Let's go!
 *
 ******************************************************************************/

app.listen(3000, function () {
    console.log('Listening on port 3000');
});

/*******************************************************************************
 *
 * Responsible for installing and rendering the Shopify app.
 *
 ******************************************************************************/

app.get('/', function (req, res) {
    if (authenticated(req)) {
        res.render('app', {
            title: app_title,
            app_api_key: app_api_key,
            shop_name: req.session.shop_name
        });
    } else {
        res.redirect('/install');
    }
});

app.get('/install', function (req, res) {
    if (authenticated(req)) {
        res.render('app', {
            title: app_title,
            app_api_key: app_api_key,
            shop_name: req.session.shop_name
        });
    } else {
        res.render('install', {
            title: app_title
        });
    }
});

app.get('/auth', function (req, res) {
    if (authenticated(req)) {
        res.render('app', {
            title: app_title,
            app_api_key: app_api_key,
            shop_name: req.session.shop_name
        });
    } else {
        if (req.query.shop) {
            req.session.shop_name = req.query.shop;
            res.render('redirect', {
                shop: req.query.shop,
                api_key: app_api_key,
                scope: scope,
                redirect_uri: redirect_uri
            });
        }
    }
});

app.get('/installed', verify_request, function (req, res) {
    if (authenticated(req)) {
        res.render('app', {
            title: app_title,
            app_api_key: app_api_key,
            shop_name: req.session.shop
        });
    } else {
        if (req.query.shop) {
            var req_body = querystring.stringify({
                client_id: app_api_key,
                client_secret: app_api_secret,
                code: req.query.code
            });
            request({
                url: 'https://' + req.query.shop + '/admin/oauth/access_token',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Content-Length': Buffer.byteLength(req_body)
                },
                body: req_body
            }, function (err, resp, body) {
                body = JSON.parse(body);
                req.session.access_token = body.access_token;
                init_script_tag(req, 'onload', app_uri + '/script-tag-script');
                init_webhook(req, 'carts/create', 'cart-created');
                init_webhook(req, 'carts/update', 'cart-updated');
                init_webhook(req, 'orders/cancelled', 'order-cancelled');
                init_webhook(req, 'orders/create', 'order-created');
                init_webhook(req, 'orders/delete', 'order-deleted');
                init_webhook(req, 'orders/fulfilled', 'order-fulfilled');
                init_webhook(req, 'orders/paid', 'order-paid');
                init_webhook(req, 'orders/partially_fulfilled', 'order-partially-fulfilled');
                init_webhook(req, 'orders/updated', 'order-updated');
                init_page(req, 'Customers', fs.readFileSync('./customers_body_html.html').toString());
                res.redirect('/');
            });
        }
    }
});

/*******************************************************************************
 *
 * Serve Script Tag script.
 *
 ******************************************************************************/

app.get('/script-tag-script', function (req, res) {
    fs.readFile(script_tag_script, function (err, data) {
        if (err) return console.log(err); // TODO
        res.send(data);
    });
});

/*******************************************************************************
 *
 * Listen for certain events.
 *
 ******************************************************************************/

app.post('/script-tag-script', function (req, res) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With');
    var sku = req.body.sku;
    sync.ensure_product_is_in_stock(sku, function (err, availability) {
        if (err) return console.log(err); // TODO
        availability = JSON.stringify({ 'availability' : availability });
        res.send(availability);
    });
});

app.post('/webhook/cart-created', function (req, res) {
    //
});

app.post('/webhook/cart-updated', function (req, res) {
    //
});

app.post('/webhook/order-cancelled', function (req, res) {
    //
});

app.post('/webhook/order-created', function (req, res) {
    //
});

app.post('/webhook/order-deleted', function (req, res) {
    //
});

app.post('/webhook/order-fulfilled', function (req, res) {
    //
});

app.post('/webhook/order-paid', function (req, res) {
    //
});

app.post('/webhook/order-partially-fulfilled', function (req, res) {
    //
});

app.post('/webhook/order-updated', function (req, res) {
    //
});

/*******************************************************************************
 *
 * Responsible for ...
 *
 ******************************************************************************/

app.get('/vendor-credentials', function (req, res) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With');
    fs.readFile(vendor_credentials, function (err, data) {
        if (err) return console.log(error); // TODO
        var credentials = JSON.parse(data);
        for (var vendor in credentials) {
            var password_length = credentials[vendor]['password'].length;
            delete credentials[vendor]['password'];
            credentials[vendor]['password_length'] = password_length;
        }
        res.send(credentials);
    });
});

app.post('/vendor-credentials', function (req, res) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With');
    var vendor_identifier = req.body.vendor_identifier;
    var new_username = req.body.new_username;
    var new_password = req.body.new_password;
    var credentials = JSON.parse(fs.readFileSync(vendor_credentials));
    setTimeout(function () {
    for (var vendor in credentials) {
        if (credentials[vendor]['identifier'] == vendor_identifier) {
            credentials[vendor]['username'] = new_username;
            credentials[vendor]['password'] = new_password;
            fs.writeFileSync(vendor_credentials, JSON.stringify(credentials, null, 4));
            var updated_credentials = credentials[vendor];
            var password_length = updated_credentials['password'].length;
            delete updated_credentials['password'];
            updated_credentials['password_length'] = password_length;
            res.send(updated_credentials);
        }
    }
    }, 3000);
});

/*******************************************************************************
 *
 * Customers.
 *
 ******************************************************************************/

// FINISH
app.post('/customer-create', function (req, res, next) {
    passport.authenticate('customer-create', function (err, customer) {
        if (err) {
            return console.log(err); // TODO
        }
        if (!customer) {
            res.send('CUSTOMER ALREADY EXISTS');
        } else {
            res.send('CUSTOMER CREATED');
        }
    })(req, res, next);
});

app.post('/customer', function (req, res) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With');
    var customer_token = req.body.customer_token;
    jwt.verify(customer_token, 'SECRET_GOES_HERE', function (err, verified_token) {
        if (err) {
            res.send(JSON.stringify({
                authenticated : false,
            }));
        } else {
            res.send(JSON.stringify({
                authenticated : true,
                customer_email : verified_token['email']
            }));
        }
    });
});

app.post('/customer-login', function (req, res) {
    passport.authenticate('customer-login', { session : false }, function (err, customer) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Headers', 'X-Requested-With');
        if (err) {
            return console.log(err);
        }
        var customer_token = null;
        var login_message = 'Invalid email or password. Please try again.';
        if (customer) {
            customer_token = jwt.sign(JSON.stringify(customer), 'SECRET_GOES_HERE');
            login_message = '';
        }
        res.send(JSON.stringify({
            'customer_token' : customer_token,
            'login_message' : login_message
        }));
    })(req, res);
});

/*******************************************************************************
 *
 * Customer authentication.
 *
 ******************************************************************************/

passport.use('customer-create', new LocalStrategy({
    usernameField : 'email',
    passwordField : 'password',
    passReqToCallback : true
}, function (req, email, password, done) {
    process.nextTick(function () {
        Customer.findOne({ 'email' : email }, function (err, customer) {
            if (err) {
                return done(err);
            }
            if (customer) {
                return done(null, false);
            } else {
                var new_customer = new Customer();
                new_customer.email = email;
                new_customer.password = new_customer.generate_hash(password);
                new_customer.save(function (err) {
                    if (err) {
                        throw err;
                    }
                    return done(null, new_customer);
                });
            }
        });
    });
}));

passport.use('customer-login', new LocalStrategy({
    usernameField : 'customer_email',
    passwordField : 'customer_password',
    passReqToCallback : true
}, function (req, email, password, done) {
    Customer.findOne({ 'email' : email }, function (err, customer) {
        if (err) {
            return done(err);
        }
        if (!customer || !customer.valid_password(password)) {
            return done(null, false);
        }
        return done(null, customer);
    });
}));

/*******************************************************************************
 *
 * Helpers.
 *
 ******************************************************************************/

function authenticated(req) {
    return req.session.access_token;
}

function customer_authenticated(req) {
    return req.session.customer_access_token;
}

function init_script_tag(req, event, src) {
    script_tag = {
        url: 'https://' + req.query.shop + '/admin/script_tags.json',
        method: 'POST',
        headers: {
            'X-Shopify-Access-Token': req.session.access_token
        },
        json: {
            "script_tag": {
                "event": event,
                "src": src
            }
        }
    };
    request(script_tag, function (err, res, body) {
        if (err) return console.log(err); // TODO
        //console.log(body);
    });
}

function init_webhook(req, topic, address) {
    address = app_uri + '/webhook/' + address;
    webhook = {
        url: 'https://' + req.query.shop + '/admin/webhooks.json',
        method: 'POST',
        headers: {
            'X-Shopify-Access-Token': req.session.access_token
        },
        json: {
            "webhook": {
                "topic": topic,
                "address": address,
                "format": "json"
            }
        }
    };
    request(webhook, function (err, res, body) {
        if (err) return console.log(err); // TODO
        //console.log(body);
    });
}

function init_page(req, title, body_html) {
    page = {
        url: 'https://' + req.query.shop + '/admin/pages.json',
        method: 'POST',
        headers: {
            'X-Shopify-Access-Token': req.session.access_token
        },
        json: {
            "page": {
                "title": title,
                "body_html": body_html
            }
        }
    };
    request(page, function (err, res, body) {
        if (err) return console.log(err); // TODO
        //console.log(body);
    });
}

function verify_request(req, res, next) {
    var map = JSON.parse(JSON.stringify(req.query));
    delete map['signature'];
    delete map['hmac'];
    var message = querystring.stringify(map);
    var generated_hash = crypto
    .createHmac('sha256', app_api_secret)
    .update(message)
    .digest('hex');
    if (generated_hash == req.query.hmac) {
        next();
    } else {
        return res.json(400);
    }
}
