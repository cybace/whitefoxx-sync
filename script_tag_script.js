(function () {
    var sku = 'CVAJA-SM120-Gold'; // TODO

    var script_tag_endpoint = 'https://5bda827f.ngrok.io/script-tag-script';
    var script_tag_req = new XMLHttpRequest();
    script_tag_req.addEventListener('load', script_tag_res);
    script_tag_req.open('POST', script_tag_endpoint);
    script_tag_req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    script_tag_req.send('sku=' + sku);

    function script_tag_res() {
        var add_to_cart_button = document.getElementById('addToCart-product-template');
        var add_to_cart_button_text = document.getElementById('addToCartText-product-template');
        var availability = JSON.parse(this.responseText);
        if (availability['availability']) {
            add_to_cart_button_text.innerHTML = 'Add To Cart';
            add_to_cart_button.disabled = false;
        } else {
            add_to_cart_button_text.innerHTML = 'Out Of Stock';
        }
    }
})();
