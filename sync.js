var no_api = require('./no_api');

module.exports = {
    ensure_product_is_in_stock: function (sku, callback) {
        no_api.chinavasion_ensure_product_is_in_stock(sku, function (err, availability) {
            callback(err, availability);
        });
    }
};
