var bcrypt = require('bcrypt-nodejs');
var mongoose = require('mongoose');

var customer_schema = mongoose.Schema({
    email : String,
    password : String
});

customer_schema.methods.generate_hash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

customer_schema.methods.valid_password = function (password) {
    return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('Customer', customer_schema);
