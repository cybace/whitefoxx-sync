window.onload = function () {
    var server_url = 'https://5bda827f.ngrok.io';

    var vendor_credentials;

    var wrapper = document.getElementById('wrapper');
    wrapper.innerHTML = '<h1>LOADING...</h1>'; // TODO

    var init_req = new XMLHttpRequest();
    init_req.responseType = 'json';
    init_req.addEventListener('load', init_ui);
    init_req.open('GET', server_url + '/vendor-credentials', true);
    init_req.send(null);
    function init_ui() {
        wrapper.innerHTML = '';

        vendor_credentials = this.response;
        for (var vendor in vendor_credentials) {
            var vendor_identifier = vendor_credentials[vendor]['identifier'];
            wrapper.innerHTML += vendor_div(vendor_identifier);

            var div = document.getElementById(vendor_identifier);
            var vendor_name = vendor;
            div.innerHTML = vendor_section_determine(vendor_name);
        }

        listen_for_button_clicks();
    }

    function vendor_div(vendor_identifier) {
        var html = '<div id=' + vendor_identifier + '></div>';
        return html;
    }

    function vendor_section_determine(vendor_name) {
        var vendor_connection = vendor_credentials[vendor_name]['connection'];
        if (vendor_connection == 0) {
            return vendor_section_disconnected(vendor_name);
        }
        return vendor_section_connected(vendor_name);
    }

    function vendor_section_edit(vendor_name) {
        var vendor_identifier = vendor_credentials[vendor_name]['identifier'];
        var vendor_username = vendor_credentials[vendor_name]['username'];
        var vendor_password_length = vendor_credentials[vendor_name]['password_length'];
        var vendor_password = '';
        for (var i = 0; i < vendor_password_length; i++) {
            vendor_password += '*';
        }
        var html =
    '<div class="section-cell">' +
        '<label>Username:</label>' +
        '<input type="text" id=' + vendor_identifier + '-new-username placeholder=' + vendor_username + '>' +
        '<label>Password:</label>' +
        '<input type="text" id=' + vendor_identifier + '-new-password placeholder=' + vendor_password + '>' +
        '<input type="button" id=' + vendor_identifier + '-edit_update class="btn primary" value="Update">' +
        '<input type="button" id=' + vendor_identifier + '-edit_cancel class="btn destroy force-right" value="Cancel">' +
    '</div>' +
    '<div class="vertical-pad"></div>';
        return html;
    }

    function vendor_section_connecting(vendor_name) {
        var vendor_identifier = vendor_credentials[vendor_name]['identifier'];
        var vendor_username = vendor_credentials[vendor_name]['username'];
        var vendor_password_length = vendor_credentials[vendor_name]['password_length'];
        var vendor_password = '';
        for (var i = 0; i < vendor_password_length; i++) {
            vendor_password += '*';
        }
        var html = 
    '<div class="section-cell">' +
        '<span class="tag gray inactive">' + vendor_name + '</span>' +
        '<span class="icon ico-check"></span>' +
        '<table class="table-section">' +
            '<tbody>' +
                '<tr>' +
                    '<td>' + vendor_username + '</td>' +
                    '<td>' + vendor_password + '</td>' +
                    '<td><input type="button" id=' + vendor_identifier + '-connecting_edit class="btn disabled force-right" value="Edit"></td>' +
                '</tr>' +
            '</tbody>' +
        '</table>' +
    '</div>' +
    '<div class="vertical-pad"></div>';
        return html;
    }

    function vendor_section_connected(vendor_name) {
        var vendor_identifier = vendor_credentials[vendor_name]['identifier'];
        var vendor_username = vendor_credentials[vendor_name]['username'];
        var vendor_password_length = vendor_credentials[vendor_name]['password_length'];
        var vendor_password = '';
        for (var i = 0; i < vendor_password_length; i++) {
            vendor_password += '*';
        }
        var html =
    '<div class="section-cell">' +
        '<span class="tag green">' + vendor_name + '</span>' +
        '<span class="icon ico-checkmark-green"></span>' +
        '<table class="table-section">' +
            '<tbody>' +
                '<tr>' +
                    '<td>' + vendor_username + '</td>' +
                    '<td>' + vendor_password + '</td>' +
                    '<td><input type="button" id=' + vendor_identifier + '-connected_edit class="btn primary force-right" value="Edit"></td>' +
                '</tr>' +
            '</tbody>' +
        '</table>' +
    '</div>' +
    '<div class="vertical-pad"></div>';
        return html;
    }

    function vendor_section_disconnected(vendor_name) {
        var vendor_identifier = vendor_credentials[vendor_name]['identifier'];
        var html =
    '<div class="section-cell">' +
        '<span class="tag pink">' + vendor_name + '</span>' +
        '<span class="icon ico-fail-red"></span>' +
        '<table class="table-section">' +
            '<tbody>' +
                '<tr>' +
                    '<td><div class="box warning"><i class="ico-warning"></i>INVALID CREDENTIALS</div></td>' +
                    '<td><input type="button" id=' + vendor_identifier + '-disconnected_edit class="btn primary force-right" value="Edit"></td>' +
                '</tr>' +
            '</tbody>' +
        '</table>' +
    '</div>' +
    '<div class="vertical-pad"></div>';
        return html;
    }

    function listen_for_button_clicks() {
        var inputs = document.getElementsByTagName('input');
        for (var input in inputs) {
            input = inputs[input];
            if (input.getAttribute('type') == 'button') {
                input.addEventListener('click', function (e) {
                    handle_button_click(e);
                });
            }
        }
    }

    function handle_button_click(e) {
        var id = e.target.id;
        var split = id.split('-');
        var vendor_identifier = split[0];
        var vendor_name;
        for (var vendor in vendor_credentials) {
            if (vendor_credentials[vendor]['identifier'] == vendor_identifier) {
                vendor_name = vendor;
                break;
            }
        }
        var action = split[1];
        if (action == 'edit_update') {
            update_vendor_credentials(vendor_name, vendor_identifier);
        }
        if (action == 'edit_cancel') {
            cancel_edit_vendor_credentials(vendor_name, vendor_identifier);
        }
        if (action == 'connecting_edit') {
            edit_vendor_credentials(vendor_name, vendor_identifier);
        }
        if (action == 'connected_edit') {
            edit_vendor_credentials(vendor_name, vendor_identifier);
        }
        if (action == 'disconnected_edit') {
            edit_vendor_credentials(vendor_name, vendor_identifier);
        }
    }

    function update_vendor_credentials(vendor_name, vendor_identifier) {
        var new_username = document.getElementById(vendor_identifier + '-new-username').value;
        var new_password = document.getElementById(vendor_identifier + '-new-password').value;
        if (new_username.length == 0 || new_password.length == 0) return;

        var update_req = new XMLHttpRequest();
        update_req.addEventListener('load', update_vendor_credentials_response);
        update_req.open('POST', server_url + '/vendor-credentials');
        update_req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        update_req.send('vendor_identifier=' + vendor_identifier + '&new_username=' + new_username + '&new_password=' + new_password);

        vendor_credentials[vendor_name]['username'] = new_username;
        vendor_credentials[vendor_name]['password_length'] = new_password.length;

        var div = document.getElementById(vendor_identifier);
        div.innerHTML = vendor_section_connecting(vendor_name);
    }

    function update_vendor_credentials_response() {
        var updated_credentials = JSON.parse(this.responseText);
        var vendor_identifier = updated_credentials['identifier'];
        for (var vendor in vendor_credentials) {
            if (vendor_credentials[vendor]['identifier'] == vendor_identifier) {
                vendor_credentials[vendor]['connection'] = updated_credentials['connection'];
                vendor_credentials[vendor]['username'] = updated_credentials['username'];
                vendor_credentials[vendor]['password_length'] = updated_credentials['password_length'];

                var div = document.getElementById(vendor_identifier);
                var vendor_name = vendor;
                div.innerHTML = vendor_section_determine(vendor_name);
                listen_for_button_clicks();
            }
        }
    }

    function cancel_edit_vendor_credentials(vendor_name, vendor_identifier) {
        var div = document.getElementById(vendor_identifier);
        div.innerHTML = vendor_section_determine(vendor_name);
        listen_for_button_clicks();
    }

    function edit_vendor_credentials(vendor_name, vendor_identifier) {
        var div = document.getElementById(vendor_identifier);
        div.innerHTML = vendor_section_edit(vendor_name);
        listen_for_button_clicks();
    }
};
